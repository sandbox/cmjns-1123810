<?php 
/**
 * @file
 *   Provides the Apachesolr response plugin class
 */
 
class ApachesolrContext_ApachesolrSearchExecute extends context_reaction {

  function options_form($context) {
    $form = array();
    
    $form['enable_cache'] = array(
      '#type' => 'checkbox',
      '#title' => 'Enable query and response caching',
      '#description' => 'Uncheck to have a new default query created and executed every time a page loads',
      '#default_value' => $context->enable_cache,
      );
      
    $form['keywords'] = array(
      '#type' => 'textfield',
      '#title' => 'Keywords',
      '#description' => 'Keywords applied to the initial query. Example: foo bar,baz',
      '#default_value' => $context->keywords,
      );
      
    $form['filter_query'] = array(
      '#type' => 'textarea',
      '#title' => 'Filter query',
      '#description' => 'Filters applied to the initial query. Example: type:story,tid:1',
      '#default_value' => $context->filter_query,
      );
      
    $form['sorts'] = array(
      '#type' => 'textarea',
      '#title' => 'Sorts',
      '#description' => 'Sorts to make available on the initial query.',
      '#default_value' => $context->sorts,
      );
      
    $form['params'] = array(
      '#type' => 'textarea',
      '#title' => 'Params',
      '#description' => 'Query parameters affecting the initial query. Example: facet.sort:FALSE',
      '#default_value' => $context->params,
      );
    
    return $form;
    
    return array('apachesolr_search_execute' => array('#type' => 'value', '#value' => TRUE));
  }

  function options_form_submit($values) {
    return array('apachesolr_search_execute' => 1);
  }
  
  function execute() {
    $contexts = context_active_contexts();
    $classes = array();

    foreach ($contexts as $k => $v) {
      if (!empty($v->reactions[$this->plugin]['apachesolr_search_execute'])) {

        ctools_include('object-cache');        
        
        $caller = 'apachesolr_search';
        $params = array();
        
        /**
         * apachesolr_drupal_query() knows about the class definition. By running it first, be make sure that
         * cached query objects get unserialized properly
         */
        $query = apachesolr_drupal_query($keys, $filterstring, $solrsort, 'search/apachesolr_search');
        
        $cached_query = ctools_object_cache_get('apachesolr_context', 'apachesolr_query');
        $cached_response = ctools_object_cache_get('apachesolr_context', 'apachesolr_response');        
                
        /**
         * If no cached query exists then execute an empty search. Our implementation of
         * hook_apachesolr_process_results() will take care of caching the query and response
         * for the next time around
         */ 
        if (!$cached_query && !$cached_response) {
          apachesolr_search_execute(NULL, NULL, NULL, 'search/apachesolr_search', $page=0);
          return;
        }
        
        /**
         * TODO: If we bother to check query and response independently of one another then we should also
         * be able to respond intelligently when one is absent but the other present. Why this would be the
         * case I don't know.
         */        
        
        if ($cached_query) {
          $query = $cached_query;
          apachesolr_current_query($query);
        }
        
        if ($cached_response) {
          apachesolr_has_searched(TRUE);
          apachesolr_static_response_cache($cached_response);
        }
      }
    }
  }
}
