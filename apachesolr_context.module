<?php
/**
 * @file
 *   Provides a context reaction for creating and persisting an Apachesolr search 
 */

/**
 * Implementation of hook_context_plugins()
 */
function apachesolr_context_context_plugins() {
  $plugins = array();
  
  $plugins['apachesolr_context_context_reaction_apachesolr_search_execute'] = array(
    'handler' => array(
      'path' => drupal_get_path('module', 'apachesolr_context') .'/plugins',
      'file' => 'apachesolr_context_context_reaction_apachesolr_search_execute.inc',
      'class' => 'ApachesolrContext_ApachesolrSearchExecute',
      'parent' => 'context_reaction',
      ),
    );
  return $plugins;
}

/**
 * Implementation of hook_context_registry()
 */
function apachesolr_context_context_registry() {
  return array(
    'reactions' => array(
      'apachesolr_search_execute' => array(
        'title' => t('Execute an Apachesolr search'),
        'plugin' => 'apachesolr_context_context_reaction_apachesolr_search_execute',
      ),
    ),
  );
}

/**
 * Implementation of hook_init()
 */
function apachesolr_context_init() {
  $plugin = context_get_plugin('reaction', 'apachesolr_search_execute');
  if ($plugin) {
    $plugin->execute();
  }
}

/**
 * Implementation of hook_apachesolr_process_results().
 * This is the only hook that runs after all the query altering hooks have been
 * run. Also, it only runs if the query is good (has results). So this is where
 * we choose to cache our query and response.
 */
function apachesolr_context_apachesolr_process_results() {
  ctools_include('object-cache');
          
  $response = apachesolr_static_response_cache();
  $query = apachesolr_current_query();
  
  ctools_object_cache_set('apachesolr_context', 'apachesolr_response', $response);
  ctools_object_cache_set('apachesolr_context', 'apachesolr_query', $query);
}